import React, { Component } from 'react';
import './App.css';

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

//Admin&login stuff 
import Login from './components/login-component/login';
import Register from './components/register-component/register';
import AdminListLocations from './components/admin-components/list-locations/list-locations';
import AdminListUsers from './components/admin-components/list-users/list-users';
import AdminListEvents from './components/admin-components/list-events/list-events';
import AdminListTickets from './components/admin-components/list-tickets/list-tickets';
import AdminCreateLocation from './components/admin-components/create-location/create-location';
import AdminEditLocation from './components/admin-components/edit-location/edit-location';

//User stuff
import UserListEvents from './components/user-components/list-events/list-events';
import UserListTickets from './components/user-components/list-tickets/list-tickets';
import UserCreateTickets from './components/user-components/create-tickets/create-tickets';
//Artist stuff

import ArtistListEvents from './components/artist-components/list-events/list-events';
import ArtistCreateEvent from './components/artist-components/create-event/create-event';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Router>
            <div>
              <Route path ='/login' component = {Login}/>
              <Route path ='/register' component = {Register}/>
              <Route path ='/admin/events' component = {AdminListEvents}/>
              <Route path ='/admin/locations' component = {AdminListLocations}/>
              <Route path ='/admin/createLocation' component = {AdminCreateLocation}/>
              <Route path ='/admin/editLocation/:id' component = {AdminEditLocation}/>
              <Route path ='/admin/users' component = {AdminListUsers}/>
              <Route path ='/admin/tickets' component = {AdminListTickets}/>
              <Route path = '/user/events/:id' component = {UserListEvents}/>
              <Route path = '/user/tickets/:id' component = {UserListTickets}/>
              <Route path = '/user/buy/:id1/:id2' component = {UserCreateTickets}/>
              <Route path = '/artist/events/:id' component = {ArtistListEvents} />
              <Route path = '/artist/createEvent/:id' component = {ArtistCreateEvent} />
            </div>
          </Router>
        </header>
      </div>
    );
  }
}

export default App;
