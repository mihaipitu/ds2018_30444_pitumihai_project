import React, {Component} from 'react';
import axios from 'axios';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            submitted: false,
            loading: false,
            type: 'default',
            id: 0
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.registerUser = this.registerUser.bind(this);
    }

    registerUser(e) {
        window.location.href = 'http://localhost:3000/register';
    }

    handleChange(e) {
        const {name, value} = e.target;
        this.setState({[name]: value});
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({submitted: true});
        const {username, password } = this.state;

        if(!(username && password)) {
            return;
        }

        const user = {'id':'0','name':'name','type':'user',username,password};
        axios.post('/api/Login/LoginUser',user)
        .then(res => {
            console.log(JSON.stringify(res.data));
            this.setState({id: res.data.id ,type: res.data.type});
        });
    }

    render() {
        const { username, password, submitted, loading, error } = this.state;
        switch(this.state.type) {
            case 'admin':
                console.log("admin");
                this.setState({type: 'default'});
                window.location.href = 'http://localhost:3000/admin/events';
                break;
            case 'user':
                console.log("user");
                this.setState({type: 'default'});
                window.location.href = 'http://localhost:3000/user/events/'+this.state.id;
                break;
            case 'artist':
                console.log("artist");
                this.setState({type: 'default'});
                window.location.href = 'http://localhost:3000/artist/events/'+this.state.id;
                break;
            default:
            return (
                <div className="col-md-6 col-md-offset-3">
                    <h2>Login</h2>
                    <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                            <label htmlFor="username">Username</label>
                            <input type="text" className="form-control" name="username" value={username} onChange={this.handleChange} />
                            {submitted && !username &&
                                <div className="help-block">Username is required</div>
                            }
                        </div>
                        <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                            <label htmlFor="password">Password</label>
                            <input type="password" className="form-control" name="password" value={password} onChange={this.handleChange} />
                            {submitted && !password &&
                                <div className="help-block">Password is required</div>
                            }
                        </div>
                        <div className="form-group">
                            <button className="btn btn-primary" disabled={loading}>Login</button>
                        </div>
                    </form>
                    <br/>
                    <button onClick={this.registerUser}>Register user</button>
                </div>
            );
                break;
        }
        
    }
}

export default Login;
