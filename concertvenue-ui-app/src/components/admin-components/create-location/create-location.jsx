import React, {Component} from 'react';

import axios from 'axios';

class CreateLocation extends Component {
    constructor(props) {
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeCity = this.onChangeCity.bind(this);
        this.onSubmit = this.onSubmit.bind(this);    

        this.state = {
            name: '',
            city :'',
            submitted : false
        }
    }

    onChangeName(e) {
        this.setState({
            name : e.target.value,
            submitted : false
        });
    }

    onChangeCity(e) {
        this.setState({
            city : e.target.value,
            submitted : false
        });
    }

    onSubmit(e) {
        e.preventDefault();

        const location = {
            id : 0,
            name : this.state.name,
            city : this.state.city
        };

        this.setState({
            submitted: true
        });

        axios.post('/api/Location',location)
        .then(res => {
            console.log(JSON.stringify(res.data));
            window.location.href = 'http://localhost:3000/admin/locations';
        });

        this.setState({
            name: '',
            city: '',
            submitted: false
        });
    }

    render() {
            return (
            <div style={{marginTop: 10}}>
                <h3>Create New Location</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group"> 
                        <label>Name: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.name}
                                onChange={this.onChangeName}
                                />
                    </div>
                    <div className="form-group">
                        <label>City: </label>
                        <input 
                                type="text" 
                                className="form-control"
                                value={this.state.city}
                                onChange={this.onChangeCity}
                                />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Create Location" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        );
    }

}

export default CreateLocation;