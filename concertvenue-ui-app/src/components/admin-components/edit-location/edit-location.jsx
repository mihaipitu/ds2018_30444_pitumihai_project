import React, {Component} from 'react';
import axios from 'axios';

class EditLocation extends Component {
    constructor(props) {
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeCity = this.onChangeCity.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        
        this.state = {
            id: this.props.match.params.id,
            name: '',
            city: '',
            location: []
        };

        this.getLocationById();
    }

    getLocationById()
    {
        axios.get('/api/Location/'+this.state.id)
        .then(res =>{
            this.setState({name: res.data.name,
            city: res.data.city});
        });
    }

    onChangeName(e) {
        this.setState({
            name : e.target.value
        });
    }

    onChangeCity(e) {
        this.setState({
            city : e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        const location = {
            id: this.state.id,
            name : this.state.name,
            city : this.state.city
        };
        
        axios.put('/api/Location/'+this.state.id, location)
        .then(res =>{
            console.log("res is: " + JSON.stringify(res.data));
            window.location.href = "http://localhost:3000/admin/locations"
        });

        this.setState({
            name: '',
            city: ''
        });
    }

    render() {
        return (
            <div style={{marginTop: 10}}>
                <h3>Edit Location</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group"> 
                        <label>Name: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.name}
                                onChange={this.onChangeName}
                                />
                    </div>
                    <div className="form-group">
                        <label>City: </label>
                        <input 
                                type="text" 
                                className="form-control"
                                value={this.state.city}
                                onChange={this.onChangeCity}
                                />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Create Location" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        );
    }

}
export default EditLocation;