import React, {Component} from 'react';
import axios from 'axios';

class ListUsers extends Component {
    constructor(props) {
        super(props);
        
        this.state = {users: []}
        this.deleteUser = this.deleteUser.bind(this);
        this.loadUsers();
    }

    loadUsers() {
    axios.get('/api/User')
      .then(res => {
            this.setState({users: res.data});
      });
    }

    deleteUser(id){
        axios.delete('/api/User/'+id)
        .then(res => {window.location.href = "http://localhost:3000/admin/users"});
      }
 

    render() {
        const TableRow = ({row}) => {
          return(
           <tr>
              <td>{row.name}</td>
              <td>{row.type}</td>
              <td>{row.username}</td>
              <td><button onClick={this.deleteUser.bind(this, row.id)}>Delete</button></td>
           </tr>
          )};
        
        const Table = ({data}) => {
          return(
          <table>
            <tr>
              <th>Name</th>
              <th>Type</th>
              <th>Username</th>
            </tr>
            {
              data.map(row => { return (
              <TableRow row={row} />)
            })}
          </table>
        )};

        return (
            <div>
              <h3 align="center">Users</h3>
              <Table data={this.state.users}/>
              <ul>
                <li><a href="http://localhost:3000/admin/locations/">Go to locations</a></li>
                <li><a href="http://localhost:3000/admin/tickets/">Go to tickets</a></li>
                <li><a href="http://localhost:3000/admin/events/">Go to events</a></li>
              </ul>
            </div>
          );
    }
}

export default ListUsers ;