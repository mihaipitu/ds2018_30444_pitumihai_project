import React, {Component} from 'react';
import axios from 'axios';

class ListLocations extends Component {
    constructor(props) {
        super(props);
        
        this.state = {locations: []}
        this.deleteLocation = this.deleteLocation.bind(this);
        this.editLocation = this.editLocation.bind(this);
        this.loadLocations();
    }

    loadLocations() {
      axios.get('/api/Location')
      .then(res => {
            console.log("From locations service: " + res.data);
            this.setState({locations: res.data});
      });
    }

    editLocation(id) {
      window.location.href = "http://localhost:3000/admin/editLocation/"+id;
    }
    deleteLocation(id){
      axios.delete('/api/Location/'+id)
      .then(res => {window.location.href = "http://localhost:3000/admin/locations"});
    }

    render() {
        
        const TableRow = ({row}) => {
          return(
             
           <tr key={row.id}>
              <td>{row.name}</td>
              <td>{row.city}</td>
              <td><button onClick={this.editLocation.bind(this, row.id)}>Edit</button></td>
              <td><button onClick={this.deleteLocation.bind(this, row.id)}>Delete</button></td>
           </tr>
          )};
        
        const Table = ({data}) => {
          return(
          <table>
            <tr>
              <th>Name</th>
              <th>Location</th>
            </tr>
            {
              data.map(row => { return (
              <TableRow row={row} />)
            })}
          </table>
        )};

        return (
            <div>
              <h3 align="center">Locations</h3>
              <Table data={this.state.locations}/>
              <ul>
                <li><a href="http://localhost:3000/admin/createLocation/">Create a new location</a></li>
                <li><a href="http://localhost:3000/admin/events/">Go to events</a></li>
                <li><a href="http://localhost:3000/admin/tickets/">Go to tickets</a></li>
                <li><a href="http://localhost:3000/admin/users/">Go to users</a></li>
              </ul>
            </div>
          );
    }
}

export default ListLocations ;