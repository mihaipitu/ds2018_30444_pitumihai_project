import React, { Component } from 'react';
import axios from 'axios';

class ListTickets extends Component {
    constructor(props) {
        super(props);
        
        this.state = {tickets: []}
        this.loadTickets();
    }

    loadTickets() {
      const axiosResp = axios.get('/api/Ticket')
      .then(res => {
            console.log("From tickets service: " + res.data);
            this.setState({tickets: res.data});
      });
    }

    render() {
        
        const TableRow = ({row}) => {
          return(
           <tr>
              <td>{row.id}</td>
              <td>{row.ticketEvent.title}</td>
              <td>{row.ticketHolder.name}</td>
           </tr>
          )};
        
        const Table = ({data}) => {
          return(
          <table>
            <tr>
              <th>Id</th>
              <th>Event</th>
              <th>Holder</th>
            </tr>
            {
              data.map(row => { return (
              <TableRow row={row} />)
            })}
          </table>
        )};

        return (
            <div>
              <h3 align="center">Tickets</h3>
              <Table data={this.state.tickets}/>
              <ul>
                <li><a href="http://localhost:3000/admin/locations/">Go to locations</a></li>
                <li><a href="http://localhost:3000/admin/events/">Go to events</a></li>
                <li><a href="http://localhost:3000/admin/users/">Go to users</a></li>
              </ul>
            </div>
          );
    }
}

export default ListTickets;