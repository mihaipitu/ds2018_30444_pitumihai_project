import React, {Component} from 'react';

import axios from 'axios';

class CreateEvent extends Component {
    constructor(props) {
        super(props);

        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeGenre = this.onChangeGenre.bind(this);
        this.onChangeDate = this.onChangeDate.bind(this);
        this.onChangeLocation = this.onChangeLocation.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeTicketPrice = this.onChangeTicketPrice.bind(this);
        this.onChangeNoTickets = this.onChangeNoTickets.bind(this);
        this.onSubmit = this.onSubmit.bind(this);    

        this.state = {
            hostId: this.props.match.params.id,
            title: '',
            genre:'',
            date:'',
            location:[],
            host:[],
            description:'',
            ticketPrice:0.0,
            noTickets:0,
            locations:[],
            submitted : false
        }

        this.loadData();
    }

    loadLocations() {
        axios.get('/api/Location')
        .then(res => {
              console.log("From create event page - locations: " + JSON.stringify(res.data));
              this.setState({locations: res.data});
        });
    }

    loadData() {
        axios.get('/api/User/'+this.state.hostId)
        .then(res => {
            console.log("From create event page - host: " + JSON.stringify(res.data));
            this.setState({host: res.data});
        });
        this.loadLocations();
    }

    onChangeTitle(e) {
        this.setState({
            title : e.target.value,
            submitted : false
        });
    }

    onChangeGenre(e) {
        this.setState({
            genre : e.target.value,
            submitted : false
        });
    }

    onChangeLocation(e) {
        axios.get('/api/Location/'+e.target.value)
        .then(res => {
            console.log("Updated location: " + JSON.stringify(res.data));
            this.setState({
                location : res.data,
                submitted : false
        })});
    }


    onChangeDate(e) {
        this.setState({
            date : e.target.value,
            submitted : false
        });
    }

    onChangeDescription(e) {
        this.setState({
            description : e.target.value,
            submitted : false
        });
    }

    onChangeTicketPrice(e) {
        this.setState({
            ticketPrice : e.target.value,
            submitted : false
        });
    }


    onChangeNoTickets(e) {
        this.setState({
            noTickets : e.target.value,
            submitted : false
        });
    }



    onSubmit(e) {
        e.preventDefault();

        const event = {
            id : 0,
            title: this.state.title,
            genre: this.state.genre,
            date: this.state.date,
            description: this.state.description,
            noTickets: this.state.noTickets,
            ticketPrice: this.state.ticketPrice,
            location: this.state.location,
            host: this.state.host
        };

        this.setState({
            submitted: true
        });

        axios.post('/api/Event',event)
        .then(res => {
            console.log(JSON.stringify(res.data));
            window.location.href = 'http://localhost:3000/artist/events/'+this.state.hostId;
        });

        this.setState({
            title: '',
            genre:'',
            date:'',
            location:[],
            description:'',
            ticketPrice:0.0,
            noTickets:0,
            submitted: false
        });
    }

    render() {
            const LocationList = ({location}) => {
                return (
                    <option key={location.id} value={location.id}>{location.name}</option>
                )
            };

            const DropDownList = ({data}) => {
                return (
                    <select value={this.state.location.id} onChange={this.onChangeLocation}>
                    {
                        data.map(location => {
                            return (
                            <LocationList location = {location}/>
                            )})
                    }
                    </select>
                )};

            return (
            <div style={{marginTop: 10}}>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group"> 
                        <label>Title: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.title}
                                onChange={this.onChangeTitle}
                                />
                    </div>
                    <div className="form-group">
                        <label>Genre: </label>
                        <input 
                                type="text" 
                                className="form-control"
                                value={this.state.genre}
                                onChange={this.onChangeGenre}
                                />
                    </div>
                    <div className="form-group">
                        <label>Date: </label>
                        <input 
                                type="text" 
                                className="form-control"
                                value={this.state.date}
                                onChange={this.onChangeDate}
                                />
                        <label>Format is: YYYY-MM-ddTHH:mm:ss</label>
                    </div>
                    <div className="form-group">
                        <label>Description: </label>
                        <input 
                                type="text" 
                                className="form-control"
                                value={this.state.description}
                                onChange={this.onChangeDescription}
                                />
                    </div>
                    <div className="form-group">
                        <label>Number of tickets available: </label>
                        <input 
                                type="text" 
                                className="form-control"
                                value={this.state.noTickets}
                                onChange={this.onChangeNoTickets}
                                />
                    </div>
                    <div className="form-group">
                        <label>Ticket price: </label>
                        <input 
                                type="text" 
                                className="form-control"
                                value={this.state.ticketPrice}
                                onChange={this.onChangeTicketPrice}
                                />
                    </div>
                    <div className="form-group">
                        <label>Location: </label>
                        <DropDownList data = {this.state.locations} />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Create Event" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        );
    }

}

export default CreateEvent;