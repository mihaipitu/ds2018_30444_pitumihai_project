import React, { Component } from 'react';
import axios from 'axios';

class ListEvents extends Component {
    constructor(props) {
        super(props);
        
        this.state = {id:this.props.match.params.id, events: []}
        this.createEvent = this.createEvent.bind(this);
        this.loadEvents();
    }

    loadEvents() {
      axios.get('/api/Event')
      .then(res => {
            console.log("From events service: " + JSON.stringify(res.data));
            this.setState({events: res.data});
      });
    }

    createEvent(id)
    {
      window.location.href = "http://localhost:3000/artist/createEvent/"+id;
    }

    render() {
        
        const TableRow = ({row}) => {
          return(
           <tr key={row.id}>
              <td>{row.title}</td>
              <td>{row.genre}</td>
              <td>{row.date}</td>
              <td>{row.location.name}</td>
              <td>{row.host.name}</td>
              <td>{row.description}</td>
              <td>{row.ticketPrice}</td>
              <td>{row.noTickets}</td>
           </tr>
          )};
        
        const Table = ({data}) => {
          return(
          <table>
            <tr>
              <th>Title</th>
              <th>Genre</th>
              <th>Date</th>
              <th>Location</th>
              <th>Host</th>
              <th>Description</th>
              <th>Ticket Price</th>
              <th>Number of Tickets Available</th>
            </tr>
            {
              data.map(row => { return (
              <TableRow row={row} />)
            })}
          </table>
        )};

        return (
            <div className="col-md-6 col-md-offset-3">
              <h3 align="center">Events</h3>
              <Table data={this.state.events}/>
              <ul>
                <li><button onClick={this.createEvent.bind(this, this.state.id)}>Create new event</button></li>
              </ul>
            </div>
          );
    }
}

export default ListEvents;