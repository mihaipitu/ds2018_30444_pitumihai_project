import React, {Component} from 'react';

import axios from 'axios';

class CreateUser extends Component {
    constructor(props) {
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onSubmit = this.onSubmit.bind(this);    

        this.state = {
            name: '',
            type: '',
            username :'',
            password:'',
            submitted : false
        }
    }

    onChangeName(e) {
        this.setState({
            name : e.target.value,
            submitted : false
        });
    }

    onChangeType(e) {
        console.log("Type is: " + e.target.value);
        this.setState({
            type : e.target.value,
            submitted : false
        });
    }

    onChangeUsername(e) {
        this.setState({
            username : e.target.value,
            submitted : false
        });
    }

    onChangePassword(e) {
        this.setState({
            password : e.target.value,
            submitted : false
        });
    }

    onSubmit(e) {
        e.preventDefault();

        const user = {
            id : 0,
            name : this.state.name,
            type : this.state.type,
            username: this.state.username,
            password: this.state.password
        };

        this.setState({
            submitted: true
        });

        axios.post('/api/Login/RegisterUser',user)
        .then(res => {
            console.log("Register user " + JSON.stringify(res.data));
            window.location.href = 'http://localhost:3000/login';
        });

        this.setState({
            name: '',
            city: '',
            submitted: false
        });
    }

    render() {
        const Types = [
            {key:'1',value:'artist'},
            {key:'2',value:'user'}
        ];
        const TypeList = ({type}) => {
            return (
                <option key={type.key} value={type.value}>{type.value}</option>
            )
        };

        const DropDownList = ({data}) => {
            return (
                <select value={this.state.type} onChange={this.onChangeType}>
                {
                    data.map(type => {
                        return (
                        <TypeList type= {type}/>
                        )})
                }
                </select>
            )};

            return (
            <div style={{marginTop: 10}}>
                <h3>Register</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group"> 
                        <label>Name: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.name}
                                onChange={this.onChangeName}
                                />
                    </div>
                    <div className="form-group">
                        <label>Type: </label>
                        <DropDownList data = {Types}/>
                    </div>
                    <div className="form-group">
                        <label>Username: </label>
                        <input 
                                type="text" 
                                className="form-control"
                                value={this.state.username}
                                onChange={this.onChangeUsername}
                                />
                    </div>
                    <div className="form-group">
                        <label>password: </label>
                        <input 
                                type="password" 
                                className="form-control"
                                value={this.state.password}
                                onChange={this.onChangePassword}
                                />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Register User" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        );
    }

}

export default CreateUser;