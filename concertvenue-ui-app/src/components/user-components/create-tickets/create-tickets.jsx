import React, {Component} from 'react';

import axios from 'axios';

class CreateTickets extends Component {
    constructor(props) {
        super(props);

        this.onChangeNoTickets = this.onChangeNoTickets.bind(this);
        this.onSubmit = this.onSubmit.bind(this);    

        this.state = {
            noTickets: 0,
            holderId: this.props.match.params.id1,
            eventId: this.props.match.params.id2,
            ticketHolder: [],
            ticketEvent: [],
            submitted : false
        }
        this.loadData();
    }

    loadData() {
        axios.get('/api/Event/'+this.state.eventId)
        .then(res => {
            console.log("From tickets service - event: " + JSON.stringify(res.data));
            this.setState({ticketEvent: res.data});
        });
        console.log("\n");
        axios.get('/api/User/'+this.state.holderId)
        .then(res => {
            console.log("From tickets service - user" + JSON.stringify(res.data));
            this.setState({ticketHolder: res.data});
      });
    }

    onChangeNoTickets(e) {
        this.setState({
            noTickets : e.target.value,
            submitted : false
        });
    }

    onSubmit = async e => {
        e.preventDefault();

        const ticket = {
            id : 0,
            ticketEvent : this.state.ticketEvent,
            ticketHolder : this.state.ticketHolder
        };

        this.setState({
            submitted: true
        });

        console.log("Ticket with data " + JSON.stringify(ticket));

        for(let i=0;i<this.state.noTickets;i++) {
            console.log("Create ticket no" + i);
            await axios.post('/api/Ticket',ticket)
            .then(res => {
                console.log(i + " " + JSON.stringify(res.data));
                this.setState({submitted: JSON.stringify(res.data)}); 
            });
        }

        if(this.state.submitted) {

            this.setState({
                ticketHolder: [],
                ticketEvent: [],
                submitted: false
            });

            window.location.href = "http://localhost:3000/user/tickets/"+this.state.holderId;
        }
    }

    render() {
            return (
            <div style={{marginTop: 10}}>
                <h3>Buy tickets</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group"> 
                        <label>Number of tickets: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.name}
                                onChange={this.onChangeNoTickets}
                                />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Create Location" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        );
    }

}

export default CreateTickets;