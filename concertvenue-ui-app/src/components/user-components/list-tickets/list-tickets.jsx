import React, { Component } from 'react';
import axios from 'axios';

class ListTickets extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
          tickets: [],
          id: this.props.match.params.id
        };
        this.getUserEvents = this.getUserEvents.bind(this);
        this.loadTickets();
    }

    loadTickets() {
     axios.get('/api/Ticket/'+this.state.id)
      .then(res => {
            console.log("From tickets service: " + res.data);
            this.setState({tickets: res.data});
      });
    }

    getUserEvents(id)
    {
      window.location.href = "http://localhost:3000/user/events/"+this.state.id;
    }

    render() {
        
        const TableRow = ({row}) => {
          return(
           <tr>
              <td>{row.id}</td>
              <td>{row.ticketEvent.title}</td>
              <td>{row.ticketHolder.name}</td>
           </tr>
          )};
        
        const Table = ({data}) => {
          return(
          <table>
            <tr>
              <th>Id</th>
              <th>Event</th>
              <th>Holder</th>
            </tr>
            {
              data.map(row => { return (
              <TableRow row={row} />)
            })}
          </table>
        )};

        return (
            <div>
              <h3 align="center">Tickets</h3>
              <Table data={this.state.tickets}/>
              <ul>
                <li><button onClick={this.getUserEvents.bind(this, this.state.id)}>Back to events</button></li>
              </ul>
            </div>
          );
    }
}

export default ListTickets;