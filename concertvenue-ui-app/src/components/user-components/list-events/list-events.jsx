import React, { Component } from 'react';
import axios from 'axios';

class ListEvents extends Component {
    constructor(props) {
        super(props);
        
        this.state = {id:this.props.match.params.id, events: []}
        this.getUserTickets = this.getUserTickets.bind(this);
        this.buyTickets = this.buyTickets.bind(this);
        this.loadEvents();
    }

    buyTickets(id){
        window.location.href = "http://localhost:3000/user/buy/"+this.state.id + "/" + id;
      }

    loadEvents() {
      axios.get('/api/Event')
      .then(res => {
            console.log("From events service: " + JSON.stringify(res.data));
            this.setState({events: res.data});
      });
    }

    getUserTickets(id)
    {
      window.location.href = "http://localhost:3000/user/tickets/"+this.state.id;
    }

    render() {
        
        const TableRow = ({row}) => {
          return(
           <tr key={row.id}>
              <td>{row.title}</td>
              <td>{row.genre}</td>
              <td>{row.date}</td>
              <td>{row.location.name}</td>
              <td>{row.host.name}</td>
              <td>{row.description}</td>
              <td>{row.ticketPrice}</td>
              <td>{row.noTickets}</td>
              <td><button onClick={this.buyTickets.bind(this, row.id)}>Buy tickets</button></td>
           </tr>
          )};
        
        const Table = ({data}) => {
          return(
          <table>
            <tr>
              <th>Title</th>
              <th>Genre</th>
              <th>Date</th>
              <th>Location</th>
              <th>Host</th>
              <th>Description</th>
              <th>Ticket Price</th>
              <th>Number of Tickets Available</th>
            </tr>
            {
              data.map(row => { return (
              <TableRow row={row} />)
            })}
          </table>
        )};

        return (
            <div className="col-md-6 col-md-offset-3">
              <h3 align="center">Events</h3>
              <Table data={this.state.events}/>
              <ul>
                <li><button onClick={this.getUserTickets.bind(this, this.state.id)}>See my tickets</button></li>
              </ul>
            </div>
          );
    }
}

export default ListEvents;