﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Models;
using ConcertVenueApp.Services.Tickets;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ConcertVenueApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private ITicketService ticketService;

        public TicketController(ITicketService ticketService)
        {
            this.ticketService = ticketService;
        }
        // GET: api/Ticket
        [HttpGet]
        public IEnumerable<Ticket> Get()
        {
            return ticketService.GetTickets();
        }

        // GET: api/Ticket/5
        [HttpGet("{id}", Name = "Ticket")]
        public IEnumerable<Ticket> Get(int id)
        {
            return ticketService.GetTicketsByUser(id);
        }

        // POST: api/Ticket
        [HttpPost]
        public bool Post([FromBody] Ticket ticket)
        {
            return ticketService.CreateTicket(ticket);
        }

        // PUT: api/Ticket/5
        [HttpPut("{id}")]
        public bool Put(int id, [FromBody] Ticket ticket)
        {
            if (id != ticket.Id)
            {
                return false;
            }
            return ticketService.UpdateTicket(ticket);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            Ticket ticket = ticketService.GetTicketById(id);
            if (ticket.Id == 0)
            {
                return false;
            }
            return ticketService.DeleteTicket(ticket);
        }
    }
}
