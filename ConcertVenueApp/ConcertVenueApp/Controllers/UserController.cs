﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Models;
using ConcertVenueApp.Services.Users.Admin;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ConcertVenueApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IAdminService adminService;

        public UserController(IAdminService adminService)
        {
            this.adminService = adminService;
        }

        [HttpGet]
        public IEnumerable<User> Get()
        {
            return adminService.GetUsers();
        }

        // GET: api/User/5
        [HttpGet("{id}", Name = "User")]
        public User Get(int id)
        {
            return adminService.GetUserById(id);
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public bool Put(int id, [FromBody] User user)
        {
            if(id != user.Id)
            {
                return false;
            }
            return adminService.UpdateUser(user);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            User user = adminService.GetUserById(id);
            if (user.Id == 0)
                return false;
            return adminService.DeleteUser(user);
        }
    }
}
