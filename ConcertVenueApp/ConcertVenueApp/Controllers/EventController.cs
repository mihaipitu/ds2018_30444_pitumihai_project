﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Models;
using ConcertVenueApp.Services.Events;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ConcertVenueApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private IEventService eventService;

        public EventController(IEventService eventService)
        {
            this.eventService = eventService;
        }

        // GET: api/Event
        [HttpGet]
        public IEnumerable<Event> Get()
        {
            return eventService.GetEvents();
        }

        // GET: api/Event/5
        [HttpGet("{id}", Name = "Event")]
        public Event Get(int id)
        {
            return eventService.GetEventById(id);
        }

        // POST: api/Event
        [HttpPost]
        public bool Post([FromBody] Event ev)
        {
            return eventService.CreateEvent(ev);
        }

        // PUT: api/Event/5
        [HttpPut("{id}")]
        public bool Put(int id, [FromBody] Event ev)
        {
            if (id != ev.Id)
            {
                return false;
            }
            return eventService.UpdateEvent(ev);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            Event ev = eventService.GetEventById(id);
            if(ev.Id == 0)
            {
                return false; 
            }
            return eventService.DeleteEvent(ev);
        }
    }
}
