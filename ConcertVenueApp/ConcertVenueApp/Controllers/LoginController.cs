﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Models;
using ConcertVenueApp.Services.Users.Login;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ConcertVenueApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private ILoginService loginService;

        public LoginController(ILoginService loginService)
        {
            this.loginService = loginService;
        }

        [HttpPost("LoginUser")]
        public User Login([FromBody]User t)
        {
            User user = loginService.LoginUser(t.Username, t.Password);
            return user;
        }

        [HttpPost("RegisterUser")]
        public bool Register([FromBody] User user)
        {
            return loginService.RegisterUser(user);
        }
    }
}