﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Models;
using ConcertVenueApp.Services.Locations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ConcertVenueApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        private ILocationService locationService;

        public LocationController(ILocationService locationService)
        {
            this.locationService = locationService;
        }

        // GET: api/Location
        [HttpGet]
        public IEnumerable<Location> Get()
        {
            return locationService.GetLocations() ;
        }

        // GET: api/Location/5
        [HttpGet("{id}", Name = "Location")]
        public Location Get(int id)
        {
            return locationService.GetLocationById(id);
        }

        // POST: api/Location
        [HttpPost]
        public bool Post([FromBody] Location location)
        {
            return locationService.CreateLocation(location);
        }

        // PUT: api/Location/5
        [HttpPut("{id}")]
        public bool Put(int id, [FromBody] Location location)
        {
            if (id != location.Id)
            {
                return false;
            }
            return locationService.UpdateLocation(location);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            Location location = locationService.GetLocationById(id);
            if (location.Id == 0)
            {
                return false;
            }
            return locationService.DeleteLocation(location);
        }
    }
}
