﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertVenueApp.Database
{
    public class DBConnection
    {
        private MySqlConnection connection = null;

        public MySqlConnection Connection { get => connection; set => connection = value; }

        public DBConnection(String connectionString)
        {
            connection = new MySqlConnection(connectionString);
        }
    }
}
