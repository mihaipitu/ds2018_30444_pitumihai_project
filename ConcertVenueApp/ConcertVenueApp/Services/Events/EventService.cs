﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Models;
using ConcertVenueApp.Repositories.EventRepo;
using ConcertVenueApp.Repositories.LocationRepo;
using ConcertVenueApp.Repositories.TicketRepo;
using ConcertVenueApp.Repositories.UserRepo;

namespace ConcertVenueApp.Services.Events
{
    public class EventService : IEventService
    {
        private IEventRepository eventRepo;
        private ILocationRepository locationRepo;
        private IUserRepository userRepo;
        private ITicketRepository ticketRepo;

        public EventService(IEventRepository eventRepo, ILocationRepository locationRepo, IUserRepository userRepo, ITicketRepository ticketRepo)
        {
            this.eventRepo = eventRepo;
            this.locationRepo = locationRepo;
            this.userRepo = userRepo;
            this.ticketRepo = ticketRepo;
        }

        public bool CreateEvent(Event ev)
        {
            ev.Id = GetMaxId() + 1;
            return eventRepo.Create(ev);
        }

        public bool DeleteEvent(Event ev)
        {
            List<Ticket> tickets = ticketRepo.FindTicketsByEvent(ev.Id);
            foreach(var ticket in tickets)
            {
                ticketRepo.Delete(ticket);
            }
            return eventRepo.Delete(ev);
        }

        public Event GetEventById(int id)
        {
            return InitEvent(eventRepo.FindById(id));
        }

        public List<Event> GetEvents()
        {
            List<Event> oldEvents = eventRepo.Read();
            List<Event> newEvents = new List<Event>();
            foreach(var ev in oldEvents)
            {
                newEvents.Add(InitEvent(ev));
            }
            return newEvents;
        }

        public List<Event> GetEventsByDescription(string description)
        {
            List<Event> oldEvents = eventRepo.FindByDescription(description);
            List<Event> newEvents = new List<Event>();
            foreach (var ev in oldEvents)
            {
                newEvents.Add(InitEvent(ev));
            }
            return newEvents;
        }

        public bool UpdateEvent(Event ev)
        {
            return eventRepo.Update(ev);
        }

        private Event InitEvent(Event ev)
        {
            Event newEv = ev;
            ev.Location = locationRepo.FindById(ev.Location_id);
            ev.Host = userRepo.FindById(ev.Host_id);
            return newEv;
        }

        private int GetMaxId()
        {
            int id = 0;
            List<Event> events = GetEvents();
            foreach (var ev in events)
            {
                if (ev.Id > id)
                    id = ev.Id;
            }
            return id;
        }
    }
}
