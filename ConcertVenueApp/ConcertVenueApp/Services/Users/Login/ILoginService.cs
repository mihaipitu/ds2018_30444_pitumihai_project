﻿using ConcertVenueApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertVenueApp.Services.Users.Login
{
    public interface ILoginService
    {
        User LoginUser(string username, string password);

        bool RegisterUser(User user);
    }
}
