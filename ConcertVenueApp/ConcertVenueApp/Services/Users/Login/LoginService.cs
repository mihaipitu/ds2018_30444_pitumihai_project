﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using ConcertVenueApp.Models;
using ConcertVenueApp.Repositories.UserRepo;

namespace ConcertVenueApp.Services.Users.Login
{
    public class LoginService : ILoginService
    {
        private IUserRepository userRepo;

        public LoginService(IUserRepository userRepo)
        {
            this.userRepo = userRepo;
        }
        
        public User LoginUser(string username, string password)
        {
            return userRepo.FindByUsernameAndPassword(username, EncodePassword(password));
        }

        public bool RegisterUser(User user)
        {
            user.Id = GetMaxId() + 1;
            user.Password = EncodePassword(user.Password);
            return userRepo.Create(user);
        }

        private int GetMaxId()
        {
            List<User> users = userRepo.Read();
            if (users.Capacity == 0)
                return 1;
            else
            {
                int max = users.ElementAt(0).Id;
                foreach (User user in users)
                {
                    if (max < user.Id)
                        max = user.Id;
                }
                return max;
            }
        }

        private string EncodePassword(string password)
        {
            SHA256 sha256 = new SHA256Managed();
            byte[] sha256Bytes = System.Text.Encoding.Default.GetBytes(password);
            byte[] cryString = sha256.ComputeHash(sha256Bytes);
            string sha256Str = string.Empty;
            foreach (byte b in cryString)
            {
                sha256Str += b.ToString("x");
            }
            return sha256Str;
        }
    }
}
