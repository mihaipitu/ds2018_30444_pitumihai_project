﻿using ConcertVenueApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertVenueApp.Services.Users.Admin
{
    public interface IAdminService
    {
        bool UpdateUser(User user);

        bool DeleteUser(User user);

        List<User> GetUsers();

        User GetUserById(int id);
    }
}
