﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using ConcertVenueApp.Models;
using ConcertVenueApp.Repositories.UserRepo;

namespace ConcertVenueApp.Services.Users.Admin
{
    public class AdminService : IAdminService
    {
        private IUserRepository userRepo;

        public AdminService(IUserRepository userRepo)
        {
            this.userRepo = userRepo;
        }

        public User GetUserById(int id)
        {
            return userRepo.FindById(id);
        }

        public bool DeleteUser(User user)
        {
            return userRepo.Delete(user);
        }

        public List<User> GetUsers()
        {
            return userRepo.Read();
        }

        public bool UpdateUser(User user)
        {
            string oldPass = userRepo.FindById(user.Id).Password;
            if (!oldPass.Equals(EncodePassword(user.Password)))
            {
                user.Password = EncodePassword(user.Password);
            }
            return userRepo.Update(user);
        }

        private string EncodePassword(string password)
        {
            SHA256 sha256 = new SHA256Managed();
            byte[] sha256Bytes = System.Text.Encoding.Default.GetBytes(password);
            byte[] cryString = sha256.ComputeHash(sha256Bytes);
            string sha256Str = string.Empty;
            foreach (byte b in cryString)
            {
                sha256Str += b.ToString("x");

            }
            return sha256Str;
        }
    }
}
