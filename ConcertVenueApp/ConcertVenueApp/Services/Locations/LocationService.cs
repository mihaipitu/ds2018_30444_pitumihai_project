﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Models;
using ConcertVenueApp.Repositories.LocationRepo;

namespace ConcertVenueApp.Services.Locations
{
    public class LocationService : ILocationService
    {
        private ILocationRepository locationRepo;

        public LocationService(ILocationRepository locationRepo)
        {
            this.locationRepo = locationRepo;
        }

        public bool CreateLocation(Location location)
        {
            location.Id = GetMaxId() + 1;
            return locationRepo.Create(location);
        }

        public bool DeleteLocation(Location location)
        {
            return locationRepo.Delete(location);
        }

        public Location GetLocationById(int id)
        {
            return locationRepo.FindById(id);
        }

        public List<Location> GetLocations()
        {
            return locationRepo.Read();
        }

        public bool UpdateLocation(Location location)
        {
            return locationRepo.Update(location);
        }

        private int GetMaxId()
        {
            int id = 0;
            List<Location> locations = GetLocations();
            foreach(var loc in locations)
            {
                if(id < loc.Id)
                {
                    id = loc.Id;
                }
            }
            return id;
        }
    }
}
