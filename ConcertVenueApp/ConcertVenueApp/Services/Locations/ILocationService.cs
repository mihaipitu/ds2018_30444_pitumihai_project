﻿using ConcertVenueApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertVenueApp.Services.Locations
{
    public interface ILocationService
    {
        bool CreateLocation(Location location);

        bool DeleteLocation(Location location);

        bool UpdateLocation(Location location);

        List<Location> GetLocations();

        Location GetLocationById(int id);
    }
}
