﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Models;
using ConcertVenueApp.Repositories.EventRepo;
using ConcertVenueApp.Repositories.TicketRepo;
using ConcertVenueApp.Repositories.UserRepo;

namespace ConcertVenueApp.Services.Tickets
{
    public class TicketService : ITicketService
    {
        private ITicketRepository ticketRepo;
        private IUserRepository userRepo;
        private IEventRepository eventRepo;

        public TicketService(ITicketRepository ticketRepo, IUserRepository userRepo, IEventRepository eventRepo)
        {
            this.ticketRepo = ticketRepo;
            this.userRepo = userRepo;
            this.eventRepo = eventRepo;
        }

        public bool CreateTicket(Ticket ticket)
        {
            ticket.Id = GetMaxId() + 1;
            return ticketRepo.Create(ticket);
        }

        public bool DeleteTicket(Ticket ticket)
        {
            return ticketRepo.Delete(ticket);
        }

        public Ticket GetTicketById(int id)
        {
            return InitTicket(ticketRepo.FindById(id));
        }

        public List<Ticket> GetTickets()
        {
            List<Ticket> oldTickets = ticketRepo.Read();
            List<Ticket> newTickets = new List<Ticket>();
            foreach(var ti in oldTickets)
            {
                newTickets.Add(InitTicket(ti));
            }
            return newTickets;
        }

        public List<Ticket> GetTicketsByUser(int user_id)
        {
            List<Ticket> oldTickets = ticketRepo.FindTicketsByHolder(user_id);
            List<Ticket> newTickets = new List<Ticket>();
            foreach (var ti in oldTickets)
            {
                newTickets.Add(InitTicket(ti));
            }
            return newTickets;
        }

        public bool UpdateTicket(Ticket ticket)
        {
            return ticketRepo.Update(ticket);
        }

        private Ticket InitTicket(Ticket ticket)
        {
            Ticket newTicket = ticket;
            newTicket.TicketEvent = eventRepo.FindById(ticket.Event_id);
            newTicket.TicketHolder = userRepo.FindById(ticket.User_id);
            return newTicket;
        }

        private int GetMaxId()
        {
            int id = 0;
            List<Ticket> tickets = GetTickets();
            foreach (var ti in tickets)
            {
                if (ti.Id > id)
                    id = ti.Id;
            }
            return id;
        }
    }
}
