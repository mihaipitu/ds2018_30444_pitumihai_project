﻿using ConcertVenueApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertVenueApp.Repositories.LocationRepo
{
    public interface ILocationRepository : IBaseRepository<Location>
    {
    }
}
