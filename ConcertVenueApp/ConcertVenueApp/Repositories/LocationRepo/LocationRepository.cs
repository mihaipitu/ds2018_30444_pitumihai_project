﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Database;
using ConcertVenueApp.Models;
using MySql.Data.MySqlClient;

namespace ConcertVenueApp.Repositories.LocationRepo
{
    public class LocationRepository : ILocationRepository
    {
        public DBConnection connectionWrapper;

        public LocationRepository(DBConnection connection)
        {
            this.connectionWrapper = connection;
        }

        public bool Create(Location t)
        {
            if (t == null)
                return false;
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Insert into location (id, name,city) VALUES('{0}', '{1}', '{2}'); ", t.Id, t.Name, t.City);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return true;
        }

        public bool Delete(Location t)
        {
            if (t == null)
                return false;
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("delete from location where id = {0};", t.Id);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return true;
        }

        public Location FindById(int id)
        {
            Location location = new Location();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from location where id = {0}", id);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        location = BuildFromReader(reader);
                    }
                }
                connection.Close();
            }
            return location;
        }

        public List<Location> Read()
        {
            List<Location> Locations = new List<Location>();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from Location");
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Locations.Add(BuildFromReader(reader));
                    }
                }
                connection.Close();
            }
            return Locations;
        }

        public bool Update(Location t)
        {
            if (FindById(t.Id) == null)
                return false;
            else
            {
                using (MySqlConnection connection = connectionWrapper.Connection)
                {
                    connection.Open();
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = String.Format("UPDATE location SET name = '{0}' ,city = '{1}' WHERE id = '{2}';", t.Name, t.City, t.Id);
                        command.ExecuteNonQuery();
                    }
                    connection.Close();
                }
                return true;
            }
        }

        private Location BuildFromReader(MySqlDataReader reader)
        {
            return new Location()
            {
                Id = reader.GetInt32(0),
                Name = reader.GetString(1),
                City = reader.GetString(2)
            };
        }
    }
}
