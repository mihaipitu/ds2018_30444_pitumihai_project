﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertVenueApp.Repositories
{
    public interface IBaseRepository<T>
    {
        T FindById(int id);

        bool Create(T t);

        List<T> Read();

        bool Update(T t);

        bool Delete(T t);
    }
}
