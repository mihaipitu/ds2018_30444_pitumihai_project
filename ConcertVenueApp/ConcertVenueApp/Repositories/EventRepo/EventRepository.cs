﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Database;
using ConcertVenueApp.Models;
using MySql.Data.MySqlClient;

namespace ConcertVenueApp.Repositories.EventRepo
{
    public class EventRepository : IEventRepository
    {
        public DBConnection connectionWrapper;

        public EventRepository(DBConnection connection)
        {
            this.connectionWrapper = connection;
        }

        public bool Create(Event t)
        {
            if (t == null)
                return false;
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Insert into event (id, title, genre, date, description, noTickets, ticketPrice, location_id, user_id) VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}'); ", t.Id, t.Title, t.Genre, t.Date.ToString("yyyy-MM-dd HH:mm:ss"), t.Description, t.NoTickets, t.TicketPrice, t.Location.Id, t.Host.Id);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return true;
        }

        public bool Delete(Event t)
        {
            if (t == null)
                return false;
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Delete from event where id = '{0}';", t.Id);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return true;
        }

        public List<Event> FindByDescription(string search)
        {
            List<Event> events = new List<Event>();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from event WHERE title LIKE '%{0}%' or genre LIKE '%{0}%' or description LIKE '%{0}%'", search);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        events.Add(BuildFromReader(reader));
                    }
                }
                connection.Close();
            }
            return events;
        }

        public Event FindById(int id)
        {
            Event ev = new Event();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from event where id = '{0}'", id);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ev = BuildFromReader(reader);
                    }
                }
                connection.Close();
            }
            return ev;
        }

        public List<Event> Read()
        {
            List<Event> events = new List<Event>();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from event ");
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        events.Add(BuildFromReader(reader));
                    }
                }
                connection.Close();
            }
            return events;
        }

        public bool Update(Event t)
        {
            if (t == null)
                return false;
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Update event SET title='{0}', genre = '{1}', date = '{2}', description = '{3}', noTickets = '{4}' , ticketPrice = '{5}' , location_id = '{6}', user_id = '{7}' where id = '{8}';", t.Title, t.Genre, t.Date.ToString("yyyy-MM-dd HH:mm:ss"), t.Description, t.NoTickets, t.TicketPrice,t.Location.Id,t.Host.Id,t.Id);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return true;
        }

        private Event BuildFromReader(MySqlDataReader reader)
        {
            return new Event()
            {
                Id = reader.GetInt32(0),
                Title = reader.GetString(1),
                Genre = reader.GetString(2),
                Date = reader.GetDateTime(3),
                Description = reader.GetString(4),
                NoTickets =reader.GetInt64(5),
                TicketPrice = reader.GetDouble(6),
                Location_id = reader.GetInt32(7),
                Host_id = reader.GetInt32(8)
            };
        }
    }
}
