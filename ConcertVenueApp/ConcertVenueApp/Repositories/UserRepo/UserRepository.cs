﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Database;
using ConcertVenueApp.Models;
using MySql.Data.MySqlClient;

namespace ConcertVenueApp.Repositories.UserRepo
{
    public class UserRepository : IUserRepository
    {
        public DBConnection connectionWrapper;

        public UserRepository(DBConnection connection)
        {
            this.connectionWrapper = connection;
        }

        public bool Create(User t)
        {
            if (t == null)
                return false;
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Insert into user(id, name, type, username, password) VALUES('{0}', '{1}', '{2}', '{3}', '{4}'); ", t.Id, t.Name, t.Type, t.Username, t.Password);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return true;
        }

        public bool Delete(User t)
        {
            if (t == null)
                return false;
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("delete from user where id = {0};", t.Id);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return true;
        }

        public User FindById(int id)
        {
            User user = new User();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from user where id = {0}", id);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        user = BuildFromReader(reader);
                    }
                }
                connection.Close();
            }
            return user;
        }

        public User FindByUsernameAndPassword(string username, string password)
        {
            User user = new User();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from user where username = '{0}' and password = '{1}' ", username, password);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        user = BuildFromReader(reader);
                    }
                }
                connection.Close();
            }
            return user;
        }

        public List<User> Read()
        {
            List<User> users = new List<User>();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from user");
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        users.Add(BuildFromReader(reader));
                    }
                }
                connection.Close();
            }
            return users;
        }

        public bool Update(User t)
        {
            if (FindById(t.Id) == null)
                return false;
            else
            {
                using (MySqlConnection connection = connectionWrapper.Connection)
                {
                    connection.Open();
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = String.Format("UPDATE user SET name = '{0}' ,username = '{1}' ,password = '{2}' WHERE id = '{3}';", t.Name, t.Username, t.Password, t.Id);
                        command.ExecuteNonQuery();
                    }
                    connection.Close();
                }
                return true;
            }
        }

        private User BuildFromReader(MySqlDataReader reader)
        {
            return new User()
            {
                Id = reader.GetInt32(0),
                Name = reader.GetString(1),
                Type = reader.GetString(2),
                Username = reader.GetString(3),
                Password = reader.GetString(4),
            };
        }
    }
}
