﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Database;
using ConcertVenueApp.Models;
using MySql.Data.MySqlClient;

namespace ConcertVenueApp.Repositories.TicketRepo
{
    public class TicketRepository : ITicketRepository
    {
        public DBConnection connectionWrapper;

        public TicketRepository(DBConnection connection)
        {
            this.connectionWrapper = connection;
        }

        public bool Create(Ticket t)
        {
            if (t == null)
                return false;
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Insert into ticket (id, user_id, event_id) VALUES('{0}', '{1}', '{2}'); ", t.Id, t.TicketHolder.Id, t.TicketEvent.Id);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return true;
        }

        public bool Delete(Ticket t)
        {
            if (t == null)
                return false;
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Delete from ticket where id ='{0}';", t.Id);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return true;
        }

        public Ticket FindById(int id)
        {
            Ticket ticket = new Ticket();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from ticket where id = '{0}'", id);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ticket = BuildFromReader(reader);
                    }
                }
                connection.Close();
            }
            return ticket;
        }

        public List<Ticket> FindTicketsByHolder(int user_id)
        {
            List<Ticket> tickets = new List<Ticket>();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from ticket where user_id = '{0}'", user_id);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        tickets.Add(BuildFromReader(reader));
                    }
                }
                connection.Close();
            }
            return tickets;
        }

        public List<Ticket> FindTicketsByEvent(int event_id)
        {
            List<Ticket> tickets = new List<Ticket>();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from ticket where event_id = '{0}'", event_id);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        tickets.Add(BuildFromReader(reader));
                    }
                }
                connection.Close();
            }
            return tickets;
        }


        public List<Ticket> Read()
        {
            List<Ticket> tickets = new List<Ticket>();
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Select * from ticket");
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        tickets.Add(BuildFromReader(reader));
                    }
                }
                connection.Close();
            }
            return tickets;
        }

        public bool Update(Ticket t)
        {
            if (t == null)
                return false;
            using (MySqlConnection connection = connectionWrapper.Connection)
            {
                connection.Open();
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = String.Format("Update ticket user_id = '{0}', event_id = '{1}' where id= '{2}'; ", t.User_id, t.Event_id, t.Id);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            return true;
        }

        private Ticket BuildFromReader(MySqlDataReader reader)
        {
            return new Ticket()
            { 
                Id = reader.GetInt32(0),
                Event_id = reader.GetInt32(1),
                User_id = reader.GetInt32(2)
            };
        }
    }
}
