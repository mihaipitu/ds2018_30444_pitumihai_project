﻿using ConcertVenueApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertVenueApp.Repositories.TicketRepo
{
    public interface ITicketRepository : IBaseRepository<Ticket>
    {
        List<Ticket> FindTicketsByHolder(int user_id);

        List<Ticket> FindTicketsByEvent(int event_id);
    }
}
