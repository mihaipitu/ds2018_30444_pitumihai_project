﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertVenueApp.Models
{
    public class Ticket
    {
        private int id;
        private int event_id;
        private int user_id;
        private Event ticketEvent;
        private User ticketHolder;

        public int Id { get => id; set => id = value; }
        public Event TicketEvent { get => ticketEvent; set => ticketEvent = value; }
        public User TicketHolder { get => ticketHolder; set => ticketHolder = value; }

        [JsonIgnore]
        public int Event_id { get => event_id; set => event_id = value; }

        [JsonIgnore]
        public int User_id { get => user_id; set => user_id = value; }
    }
}
