﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertVenueApp.Models
{
    public class Event
    {
        private int id;
        private string title;
        private string genre;
        private DateTime date;
        private string description;
        private long noTickets;
        private double ticketPrice;
        private Location location;
        private User host;
        private int location_id;
        private int host_id;

        public int Id { get => id; set => id = value; }
        public string Title { get => title; set => title = value; }
        public string Genre { get => genre; set => genre = value; }
        public DateTime Date { get => date; set => date = value; }
        public string Description { get => description; set => description = value; }
        public long NoTickets { get => noTickets; set => noTickets = value; }
        public double TicketPrice { get => ticketPrice; set => ticketPrice = value; }
        public Location Location { get => location; set => location = value; }
        public User Host { get => host; set => host = value; }


        [JsonIgnore]
        public int Location_id { get => location_id; set => location_id = value; }
        
        [JsonIgnore]
        public int Host_id { get => host_id; set => host_id = value; }
       
    }
}
