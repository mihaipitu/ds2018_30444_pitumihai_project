﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ConcertVenueApp.Models
{
    public class User
    {
        private int id;
        private string name;
        private string type;
        private string username;
        private string password;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Type { get => type; set => type = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }

    }
}
