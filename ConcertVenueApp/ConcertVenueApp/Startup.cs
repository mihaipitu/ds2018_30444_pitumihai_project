﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConcertVenueApp.Database;
using ConcertVenueApp.Repositories.EventRepo;
using ConcertVenueApp.Repositories.LocationRepo;
using ConcertVenueApp.Repositories.TicketRepo;
using ConcertVenueApp.Repositories.UserRepo;
using ConcertVenueApp.Services.Events;
using ConcertVenueApp.Services.Locations;
using ConcertVenueApp.Services.Tickets;
using ConcertVenueApp.Services.Users.Admin;
using ConcertVenueApp.Services.Users.Login;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ConcertVenueApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddTransient<DBConnection>(_ => new DBConnection(Configuration.GetConnectionString("DefaultConnection")));

            //Repos
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ILocationRepository, LocationRepository>();
            services.AddScoped<IEventRepository, EventRepository>();
            services.AddScoped<ITicketRepository, TicketRepository>();

            //Services
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<IEventService, EventService>();
            services.AddScoped<ITicketService, TicketService>();
            services.AddScoped<ILocationService, LocationService>();

            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseCors(
                builder => builder.WithOrigins("http://localhost:52095").AllowAnyMethod()
            );
        }
    }
}
